import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { sort } from '@ember/object/computed';
import { task } from 'ember-concurrency';

export default Controller.extend({
  sortProperty: 'fullName',
  
  customersSortProps: computed('sortProperty', function() {
    if(this.sortProperty == 'fullName') {
      return ['firstName', 'lastName'];
    } else {
      return [this.sortProperty];
    }
  }),
  
  sortedCustomers: sort('model', 'customersSortProps'),

  init() {
    this._super(...arguments);
    this.set('selectedFilter', 'firstName');
  },

  actions: {
    onSearchChange() {
      this.get('filterModel').perform();
    }
  },

  filterModel: task(function*() {
    let query = this.get('query');
    let modelTemp = this.get('sortedCustomers');
    let valuePath = this.get('selectedFilter');
    let result = modelTemp;

    if(query !== '') {
      result = modelTemp.filter((m) => {
        return m.get(valuePath).toLowerCase().includes(query.toLowerCase());
      });
    }

    yield this.set('sortedCustomers', result);
  }).restartable(),
});
