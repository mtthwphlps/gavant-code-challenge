import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import Table from 'ember-light-table';

export default Component.extend({
  router: service(),
  classNames: ['customers-table', 'table-responsive'],
  model: null,
  table: null,

  columns: computed(function() {
    return [
      {
        label: 'Name',
        valuePath: 'fullName',
        sortable: false
      },
      {
        label: 'Company',
        valuePath: 'company',
        sortable: false
      },
      {
        label: 'Budget',
        valuePath: 'budget',
        sortable: false
      }
    ]
  }),

  init() {
    this._super(...arguments);
    
    let topCustomers = this.model.slice(0, 5);
    var processedCustomers = [];

    for(var key in topCustomers) {
      if(topCustomers.hasOwnProperty(key)) {
        var budgetFormatted = topCustomers[key].budget + '';
        budgetFormatted = budgetFormatted.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");

        processedCustomers.push({
          fullName: topCustomers[key].firstName + ' ' + topCustomers[key].lastName,
          company: topCustomers[key].company,
          budget: "$" + budgetFormatted
        });
      }
    }

    let table = new Table(this.columns, processedCustomers, {enableSync: true});
    this.set('table', table);
  }

  
});
