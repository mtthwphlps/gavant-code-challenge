import Component from '@ember/component';

export default Component.extend({
  init() {
    this._super(...arguments);

    var totalBudgeted = 0;
    for(var key in this.customers) {
      if(!isNaN(this.customers[key].budget)) {
        totalBudgeted = totalBudgeted + parseInt(this.customers[key].budget);
      }
    }

    if(totalBudgeted > 1000000) {
      totalBudgeted = totalBudgeted / 1000000;
      totalBudgeted = parseFloat(totalBudgeted).toFixed(1);
      totalBudgeted = totalBudgeted + 'M';
    }

    this.set('totalBudgeted', totalBudgeted);
  }
});
