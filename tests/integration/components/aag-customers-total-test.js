import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | aag-customers-total', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });
    this.set('sortedCustomers', [1, 2, 3]);
    await render(hbs`<AagCustomersTotal @custTotal={{this.sortedCustomers.length}} />`);

    assert.ok(this.element.textContent);
  });
});
